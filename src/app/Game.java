package app;

import java.util.ArrayList;

import piece.*;

/**
 * This class is an instance of a running game of chess. It contains methods for 
 * maintaining the state of the board and pieces within as well as moves that can be made on the board.
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class Game {
	
	public Piece[][] board; 
	boolean whiteTurn = true;
	boolean checking = false;
	
	
	/**
	 * No-arg constructor that initializes the board as specified in the assignment spec.
	 * It adds pieces to the board as well as nulls (which is the absence of a piece)
	 * @author Elby (egb37)
	 */
	public Game(){
		board = new Piece[8][8];
		
		//Black side (its at the top
		board[0][0] = new Rook(false);
		board[0][1] = new Knight(false);
		board[0][2] = new Bishop(false);
		board[0][3] = new Queen(false);
		board[0][4] = new King(false);
		board[0][5] = new Bishop(false);
		board[0][6] = new Knight(false);
		board[0][7] = new Rook(false);
		
		for(int i = 0; i < 8; i++){
			board[1][i] = new Pawn(false);
		}
		
		//Onoccupied space is taken up by nulls;
		for(int i = 0; i < 8; i++){
			board[2][i] = null;
			board[3][i] = null;
			board[4][i] = null;
			board[5][i] = null;
		}

		//White side
		for(int i = 0; i < 8; i++){
			board[6][i] = new Pawn(true);
		}
		
		board[7][0] = new Rook(true);
		board[7][1] = new Knight(true);
		board[7][2] = new Bishop(true);
		board[7][3] = new Queen(true);
		board[7][4] = new King(true);
		board[7][5] = new Bishop(true);
		board[7][6] = new Knight(true);
		board[7][7] = new Rook(true);
		
	}
	
	/**
	 * printBoard() prints the chess board as specified in the instructions.
	 * To be called before or after a move and keeps the players aware of the current state of the board
	 * This method expects no parameters and returns no value.
	 *
	 * @author Elby (egb37)
	 */
	public void printBoard(){
		boolean printBlack = false;
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				
				if(this.board[i][j] != null){
					System.out.print(this.board[i][j] + " ");
				}else{
					if(printBlack)
						System.out.print("## ");
					else
						System.out.print("   ");
				}
				printBlack = !printBlack;

			}
			System.out.println(8-i);
			printBlack = !printBlack;
		}
		
		char c = 'a';

		for(int i = 0; i < 8; i++){
			System.out.print(" " + c + " ");
			c++;
		}
		System.out.println();
		System.out.println();

	}
	
	/**
	 * This method checks to see if a move is valid. If the move is valid, it will update the board
	 * and return true. This involves checking if the move puts this player's king in check, which is an invalid move.
	 * If the move is invalid, no move is made and returns false.
	 *
	 * @author Elby (egb37)
	 * @param startCoord	the coordinates on the board of the piece to be moved
	 * @param endCoord	the coordinates on the board of the destination of the move
	 * @param whiteMove	specifies which player's turn it is
	 * @return true if the move is valid and the board is updated, false if invalid.
	 */
	public boolean move(int[] startCoord, int[] endCoord, boolean whiteMove){
		
		Piece thisPiece = board[startCoord[0]][startCoord[1]];

		if(thisPiece == null){
			return false;
		}else if((thisPiece.isWhite && whiteMove) || (!thisPiece.isWhite && !whiteMove)){
			if(thisPiece.legalMove(startCoord, endCoord, this)){
				//Check if this move puts the player in check
				Piece thatPiece = board[endCoord[0]][endCoord[1]];
		
				board[startCoord[0]][startCoord[1]] = null;
				board[endCoord[0]][endCoord[1]] = thisPiece;
				
				for(int i = 0; i < 8; i++){
					for(int j = 0; j < 8; j++){
						int[] tempStart = {i, j};
						if(isCheck(tempStart, whiteMove)){
							//If king goes into check, replace move
							board[startCoord[0]][startCoord[1]] = thisPiece;
							board[endCoord[0]][endCoord[1]] = thatPiece;		
							return false;
						}
					}
				}
				return true;
			}else
				return false;
		}
		return false;
	}
	
	/**
	 * This method translates an input string from player into a set of coordinates used by other methods.
	 * @author Alex (aer112)
	 * @param str	The string to be translated
	 * @return an array of cooridnates from [0,8] in the form [y][x] - file, rank.
	 */
	public static int[] getIndex(String str){
		int x = 0, y;
		int[] coordinate = new int[2];
		
		if(str.charAt(0) == 'a'){
			x = 0;
		}else if(str.charAt(0) == 'b'){
			x = 1;
		}else if(str.charAt(0) == 'c'){
			x = 2;
		}else if(str.charAt(0) == 'd'){
			x = 3;
		}else if(str.charAt(0) == 'e'){
			x = 4;
		}else if(str.charAt(0) == 'f'){
			x = 5;
		}else if(str.charAt(0) == 'g'){
			x = 6;
		}else if(str.charAt(0) == 'h'){
			x = 7;
		}
		
		y = 8 - (str.charAt(1)-'0');
		
		coordinate[0] = y;
		coordinate[1] = x;
	
		return coordinate;
	}
	
	/**
	 * This method determines if a recently moved piece puts a king into check.
	 * 
	 * @author Elby (egb37)
	 * @param startCoord	The coordinates of the piece potentially putting a king in check
	 * @param white 	    True for checking if the white king is in check, false if checking for black king in check.
	 * @return true if the specified piece puts the specified king into check, false otherwise
	 */
	public boolean isCheck(int[] startCoord, boolean white){
		int[] endCoord = getKingCoordinates(white);


		Piece srcPiece = board[startCoord[0]][startCoord[1]];
		if(srcPiece == null) return false;
		checking = true;
		if(srcPiece.legalMove(startCoord, endCoord, this)){
			checking = false;
			return true;
		}
		checking = false;
		return false;
	}
	
	/**
	 * Determines the end of the game. Essentially checks if the king specified can make a legal move to get out of check
	 * returns false if ANY possible move does not put king back into check
	 * 
	 * @author Elby (egb37)
	 * @param white 	    True for checking if the white king is in checkmate, false if checking for black king in checkmate.
	 * @return true if no moves are legal, thus checkmate and false otherwise
	 */
	public boolean isCheckMate(boolean white){
		
		int[] kingCoord = getKingCoordinates(white);
		int[] tempCoord = {kingCoord[0], kingCoord[1]};
		
		//going around the square starting at the left position to the king
		//Doesn't matter if black or white because checks if valid
		//All movements up
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
				
		tempCoord[0] = tempCoord[0] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;

		tempCoord[1] = tempCoord[1] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
	
		tempCoord[1] = tempCoord[1] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[0] = tempCoord[0] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[0] = tempCoord[0] + 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		tempCoord[1] = tempCoord[1] - 1;
		if(kingCanMove(kingCoord, tempCoord, white)) return false;
		
		//Can something take the piece putting it in check?
		
		ArrayList<int[]> dangerousPieces = new ArrayList<int[]>();
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				int[] tempStart = {i, j};
				Piece dangerousPiece = board[i][j];
				if(dangerousPiece != null && dangerousPiece.legalMove(tempStart, kingCoord, this)){
					dangerousPieces.add(tempStart);
				}
			}
		}
		
		//You can't dodge this!
		if(dangerousPieces.size() > 1) return true;
		
		if(!dangerousPieces.isEmpty()){
			int[] danger = dangerousPieces.get(0);
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					int[] saviorStart = {i, j};
					Piece saviorPiece = board[i][j];
					if(saviorPiece != null && saviorPiece.legalMove(saviorStart, danger, this)){
						return false; //Something can save it
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Iteratively finds the coordinates of the king in question
	 * 
	 * @author Elby (egb37)
	 * @param white	true if finding white king, false if finding black king
	 * @return coordinates of the king of the color specified by the parameter
	 */
	public int[] getKingCoordinates(boolean white){
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				Piece thisPiece = board[i][j];
				if(thisPiece != null){
					if(King.class.isInstance(thisPiece)){
						int[] coord = {i, j};
						if(white && thisPiece.isWhite){
							return coord;
						}else if(!white && !thisPiece.isWhite){
							return coord;
						}
					}
				}
			}
		}
		return null;
	}
	/**
	 * This method unvalidates en passant after one turn has passed, as en passant is a 1-turn possibility 
	 * that extinguishes on the next turn of the player who's pawn moved two spaces.
	 * The check is done on all pieces in a row and sets their "enp" values to false, as at that point
	 * the possibility to capture that piece via en passante will have passed
	 * 
	 * @author Alex (aer112)
	 * @param whiteTurn checks if it's white's turn; to keep track of which [y] value to use when checking
	 */
	public void enPassantCheck(boolean whiteTurn){
		if(whiteTurn){
			for(int i = 0; i < 8; i++){
				if(board[4][i] != null && board[4][i].isWhite){
					board[4][i].enp = false;
				}
			}
		}else{
			for(int i = 0; i < 8; i++){
				if(board[3][i] != null && !board[3][i].isWhite){
					board[3][i].enp = false;
				}
			}
		}
	}

	/**
	 * Determines if a king can make a specific move. Used in conjunction with the isCheckMate.
	 * Simulates a move that a king might be able to make and determines if the move can be made.
	 * 
	 * @author Elby (egb37)
	 * @param startCoord	coords of the starting position of the king 
	 * @param endCoord	coords of the potential ending position of the king 
	 * @param white		true for white king, false for black king

	 * @return true if the king can make the move, false if the move is invalid or puts the king in check
	 */	
	public boolean kingCanMove(int[] startCoord, int[] endCoord, boolean white){

		if(endCoord[0] < 0 || endCoord[0] > 7 || endCoord[1] < 0 || endCoord[1] > 7) 
			return false;

		//King origional position
		Piece thisPiece = board[startCoord[0]][startCoord[1]];

		//Check if this move puts the player in check
		Piece thatPiece = board[endCoord[0]][endCoord[1]];
		
		//Empty space that king can move into
		if(thatPiece != null)
			return false;

		checking = true;
		if(thisPiece.legalMove(startCoord, endCoord, this)){
			board[startCoord[0]][startCoord[1]] = null;
			board[endCoord[0]][endCoord[1]] = thisPiece;
			checking = false;
			for(int i = 0; i < 8; i++){
				for(int j = 0; j < 8; j++){
					int[] tempStart = {i, j};
					if(isCheck(tempStart, white)){
												
						//Replace king and announce bad move
						board[startCoord[0]][startCoord[1]] = thisPiece;
						board[endCoord[0]][endCoord[1]] = thatPiece;	

						return false;
					}
				}
			}
			
			//Replace king and announce possible move
			board[startCoord[0]][startCoord[1]] = thisPiece;
			board[endCoord[0]][endCoord[1]] = thatPiece;	
			checking = false;
			return true;
		}else{
			checking = false;
			return false;
		}

	}
	public boolean getChecking(){
		return checking;
	}
}


