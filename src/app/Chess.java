package app;

import java.util.Scanner;

import piece.*;
/**
 * This class contains the runnable game and user-interface. 
 *
 * @author Elby (egb37)
 * @author Alex (aer112)
 */
public class Chess {

	public static void main(String[] args) {
		
		Game game = new Game();
		boolean whiteTurn = true;
		
        Scanner scanner = new Scanner(System.in);
        
        boolean whiteCheck = false;
        boolean blackCheck = false;
        boolean draw = false;

		while(true){
			System.out.println();
			game.printBoard();
			if(whiteTurn){
				game.enPassantCheck(true);
				System.out.print("White's move: ");
			}else{
				game.enPassantCheck(false);
				System.out.print("Black's move: ");
			}
			String input = scanner.nextLine();
			
			while(!validInput(input)){
				System.out.print("Not valid input. Try again: ");
				input = scanner.nextLine();
			}
			
			if("resign".equals(input)){
				if(!whiteTurn)System.out.print("White wins");
				else System.out.print("Black wins");
				return;
			}else if(input.contains("draw")){
				if(draw){
					System.out.println("Draw");
					return;
				}else{
					draw = true;
				}
			}else{
				draw = false;
			}
			
			String src = input.substring(0, 2);
			String dest = input.substring(3);
			
			int[] startCoord = game.getIndex(src);
			int[] endCoord = game.getIndex(dest);

			while(!game.move(startCoord, endCoord, whiteTurn)){
				System.out.print("Illegal move, try again: ");

				input = scanner.nextLine();
				while(!validInput(input)){
					System.out.println();
					System.out.print("Not valid input. Try again: ");
					input = scanner.nextLine();
				}
				
				if("resign".equals(input)){
					if(!whiteTurn)System.out.print("White wins");
					else System.out.print("Black wins");
					return;
				}else if(input.contains("draw")){
					if(draw){
						System.out.println("Draw");
						return;
					}else{
						draw = true;
					}
				}else{
					draw = false;
				}
				
				src = input.substring(0, 2);
				dest = input.substring(3);
				startCoord = game.getIndex(src);
				endCoord = game.getIndex(dest);
			}
			
			//Check for promotion
			Piece movedPiece = game.board[endCoord[0]][endCoord[1]];
			
			if(Pawn.class.isInstance(movedPiece)){
				char type = '0';
				char color = '0';
				if(input.length() == 7) type = input.charAt(6);
				if(endCoord[0] == 0 && movedPiece.isWhite) color = 'W';
				else if(endCoord[0] == 7 && !movedPiece.isWhite) color = 'B';
				
				Piece promoted = promotePiece(type, color); 
				if(promoted != null){
					game.board[endCoord[0]][endCoord[1]] = promoted;
				}
			}

			
			if(game.isCheck(endCoord, true) && !whiteTurn){
				System.out.println("Check");
				if(game.isCheckMate(true)){
					game.printBoard();
					System.out.println("Checkmate");
					System.out.print("Black wins");
					return;
				}
				whiteCheck = false;
			}else if(game.isCheck(endCoord, false) && whiteTurn){
				System.out.println("Check");
				if(game.isCheckMate(false)){
					game.printBoard();
					System.out.println("Checkmate");
					System.out.print("White wins");
					return;
				}
			}
			whiteTurn = !whiteTurn;
		}
	}
	
	/**
	 *Aids in the promotion of a piece. Returns the requested type of piece based on parameters.
	 *
	 * @author Elby (egb37)
	 * @param type	a char that determines which type of piece to be created
	 * @param color	a char that determines which color piece to be created, if '0' then return no new piece

	 * @return The requested type of piece based on color and type. Queen if no type is specified and null if no color is specified
	 */
	private static Piece promotePiece(char type, char color) {
		if(color == '0') return null;
		
		boolean white;
		if(color == 'W') 
			white = true;
		else
			white = false;
		
		if(Character.toUpperCase(type) == 'R')
			return new Rook(white);
		if(Character.toUpperCase(type) == 'N')
			return new Knight(white);
		if(Character.toUpperCase(type) == 'B')
			return new Bishop(white);
		if(Character.toUpperCase(type) == 'P')
			return new Pawn(white);
		
		return new Queen(white);
	}


	/**
	 *This method determines if a string entered by the user is a valid command for the chess game.
	 *These can be a set of coordinates, "resign" or "draw"
	 *Does not account for the draw prompt because the main method handles that
	 *
	 * @author Elby (egb37)
	 * @param input	the string which must be scanned for validity
	 * @return true if the input is valid and can be presented to the rest of the program, false otherwise
	 */
	
	public static boolean validInput(String input){
		
		input = input.toLowerCase();
		if(input.length() == 0) return false;
		
		if("resign".equals(input)) return true;
		if("draw".equals(input)) return true;
		
		if(!Character.isAlphabetic(input.charAt(0))) return false;
		if(input.charAt(0) < 'a' || input.charAt(0) > 'h') return false;
		
		if(!Character.isDigit(input.charAt(1))) return false;
		if(input.charAt(1) < '1' || input.charAt(1) > '8') return false;

		if(!Character.isAlphabetic(input.charAt(3))) return false;
		if(input.charAt(3) < 'a' || input.charAt(3) > 'h') return false;

		if(!Character.isDigit(input.charAt(4))) return false;
		if(input.charAt(4) < '1' || input.charAt(4) > '8') return false;
		
		return true;
	}
		
}
