package piece;

import app.Game;
/**
 * Implements the Knight piece
 * 
 * @author Alex (aer112)
 *
 */
public class Knight extends Piece {

	public Knight(boolean isWhite) {
		super(isWhite);
	}

	
	public String toString(){
		if(isWhite){
			return "wN";
		}
		return "bN";
	}

	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * Make's sure that the movement is in the "L" pattern and that it is not attacking a friendly piece.
	 * 
	 * @author Alex (aer112)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) {
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		//NEED TO CHECK THE ORDER OF [X][Y]
		Piece destPiece = game.board[destY][destX];
		if(destPiece == null){
			if(srcX + 1 == destX || srcX -1 == destX){
				if(srcY - 2 == destY || srcY + 2 == destY){
					return true;
				}
			}else if(srcX + 2 == destX || srcX -2 == destX){
				if(srcY -1 == destY || srcY +1 == destY){
					return true;
				}
			}
		}else{//attacking
			if(isWhite && destPiece.isWhite) return false;
			if(!isWhite && !destPiece.isWhite) return false;
			if(srcX + 1 == destX || srcX -1 == destX){
				if(srcY - 2 == destY || srcY + 2 == destY){
					return true;
				}
			}else if(srcX + 2 == destX || srcX -2 == destX){
				if(srcY -1 == destY || srcY +1 == destY){
					return true;
				}
			}
		}
		return false;
	}

}