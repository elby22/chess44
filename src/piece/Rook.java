package piece;

import app.Game;
/**
 * 
 * Implements Rook
 * 
 * @author Alexr0
 *
 */
public class Rook extends Piece{

	public Rook(boolean isWhite) {
		super(isWhite);
	}

	
	public String toString(){
		if(isWhite){
			return "wR";
		}
		return "bR";
	}

	/**
	 * Checks if a move is legal given the starting and ending coordinates.
	 * Checks for horizontal and vertical movement, attacking friendly pieces, 
	 * and if anything is in the way between the starting and ending points.
	 * @author Alex (aer112)
	 * 
	 * @param src	coords of the starting position of the piece
	 * @param dest	coords of the destination 
	 * @param game	an instance of a game to determine valid moves
	 * @return true if the move if legal
	 * @return false if the move is illegal
	 */
	public boolean legalMove(int[] src, int[] dest, Game game) {
		boolean right, up, vertical;
		int srcX = src[1];
		int srcY = src[0];
		int destX = dest[1];
		int destY = dest[0];
		
		//either moving to same spot OR not moving in a straight line horizontally or vertically
		if(srcX == destX && srcY == destY){
			return false;
		}
		
		//Diagonal movement
		if(srcX != destX && srcY != destY){ 
			return false;
		}
		
		//NEED TO CHECK THE ORDER OF [X][Y]
		Piece destPiece = game.board[destY][destX];
		if(srcY != destY){
			vertical = true;
		}else{
			vertical = false;
		}
		if(srcX > destX){
			right = false;
		}else{
			right = true;
		}
		
		if(srcY > destY){
			up = true;
		}else{
			up = false;
		}
		
		if(destPiece == null){ //not attacking
			if(vertical == true){
				if(up == true){ //checks vertically moving upwards; makes sure nothing is in the way
					for(int i = srcY-1; i > destY; i--){
						if(game.board[i][destX] != null){
							return false;
						}
					}
				}else{ //checks vertically moving downwards; makes sure nothing is in the way
					for(int i = srcY+1; i < destY; i++){
						if(game.board[i][destX] != null){
							return false;
						}
					}
				}
			}else{
				if(right == true){ //checks horizontally moving right; makes sure nothing is in the way
					for(int i = srcX+1; i < destX; i++){
						if(game.board[destY][i] != null){
							return false;
						}
					}
				}else{  //checks horizontally moving left; makes sure nothing is in the way
					for(int i = srcX-1; i > destX; i--){
						if(game.board[destY][i] != null){
							return false;
						}
					}
				}
			}
		}else{
			//Attacking same side?
			if(isWhite && destPiece.isWhite) return false;
			if(!isWhite && !destPiece.isWhite) return false;
			if(vertical == true){
				if(up == true){ //checks vertically moving upwards; makes sure nothing is in the way
					for(int i = srcY-1; i > destY; i--){
						if(game.board[i][destX] != null){
							return false;
						}
					}
				}else{ //checks vertically moving downwards; makes sure nothing is in the way
					for(int i = srcY+1; i < destY; i++){
						if(game.board[i][destX] != null){
							return false;
						}
					}
				}
			}else{
				if(right == true){ //checks horizontally moving right; makes sure nothing is in the way
					for(int i = srcX+1; i < destX; i++){
						if(game.board[destY][i] != null){
							return false;
						}
					}
				}else{  //checks horizontally moving left; makes sure nothing is in the way
					for(int i = srcX-1; i > destX; i--){
						if(game.board[destY][i] != null){
							return false;
						}
					}
				}
			}
		}
		isFirstMove = false;
		return true;
	}
}